import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './Inventory/ManufacturersList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelsList from './Inventory/ModelsList';
import ModelForm from './Inventory/ModelForm';
import AutomobilesList from './Inventory/AutomobilesList';
import AutomobileForm from './Inventory/AutomobileForm';
import SalespeopleList from './Sales/SalespeopleList';
import SalespeopleForm from './Sales/SalespeopleForm';
import CustomersList from './Sales/CustomersList';
import CustomersForm from './Sales/CustomersForm';
import SalesList from './Sales/SalesList';
import SalesForm from './Sales/SalesForm';
import SalesPersonHistory from './Sales/SalespersonHistory';
import TechnicianList from './Service/TechnicianList';
import TechnicianForm from './Service/TechnicianForm';
import ServiceAppointmentForm from './Service/ServiceAppointmentForm';
import ServiceAppointmentList from './Service/ServiceAppointmentList';
import Footer from './Footer';
import InventoryDashboard from './Inventory/InventoryDashboard';
import SalesDashboard from './Sales/SalesDashboard';
import ServiceDashboard from './Service/ServiceDashboard';

function App() {
  return (
    <BrowserRouter>
      <div className="app-container">
        <Nav />
        <div className="content">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="inventory-dashboard/" element={<InventoryDashboard />} />
            <Route path="sales-dashboard/" element={<SalesDashboard />} />
            <Route path="service-dashboard/" element={<ServiceDashboard />} />
            <Route path="manufacturers/">
              <Route path="create" element={<ManufacturerForm />} />
              <Route index element={<ManufacturersList />} />
            </Route>
            <Route path="models/">
              <Route path="create" element={<ModelForm />} />
              <Route index element={<ModelsList />} />
            </Route>
            <Route path="automobiles/">
              <Route path="create" element={<AutomobileForm />} />
              <Route index element={<AutomobilesList />} />
            </Route>
            <Route path="technicians/">
              <Route path="create" element={<TechnicianForm />} />
              <Route index element={<TechnicianList />} />
            </Route>
            <Route path="appointments/">
              <Route path="create" element={<ServiceAppointmentForm />} />
              <Route index element={<ServiceAppointmentList />} />
            </Route>
            <Route path="salespeople/">
              <Route path="create" element={<SalespeopleForm />} />
              <Route index element={<SalespeopleList />} />
            </Route>
            <Route path="customers/">
              <Route path="create" element={<CustomersForm />} />
              <Route index element={<CustomersList />} />
            </Route>
            <Route path="sales/">
              <Route path="create" element={<SalesForm />} />
              <Route index element={<SalesList />} />
              <Route path="history" element={<SalesPersonHistory />} />
            </Route>
          </Routes>
        </div>
        </div>
        < Footer />
    </BrowserRouter>
  );
}

export default App;
