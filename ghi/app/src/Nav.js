import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark py-4">
      <div className="container-fluid mx-3">
        <NavLink className="navbar-brand m-1" to="/">
          DealerSense
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="mx-5 collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav">
          <li>
              <NavLink
                className="nav-link m-1"
                aria-current="page"
                to="/"
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link m-1"
                aria-current="page"
                to="/inventory-dashboard"
              >
                Inventory
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link m-1"
                aria-current="page"
                to="/sales-dashboard"
              >
                Sales
              </NavLink>
            </li>
            <li>
              <NavLink
                className="nav-link m-1"
                aria-current="page"
                to="/service-dashboard"
              >
                Service
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
