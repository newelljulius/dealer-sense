import React, { useEffect, useState } from "react";

function ModelEdit({ id, onClose, onModelEdited }) {
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");
  const [currentManufacturer, setCurrentManufacturer] = useState(null);
  const [picture_url, setPicture_url] = useState("");

  const fetchManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const fetchModelData = async () => {
    try {
      const response = await fetch(`http://localhost:8100/api/models/${id}`);
      if (response.ok) {
        const data = await response.json();
        const { name, picture_url, manufacturer } = data;
        setName(name);
        setCurrentManufacturer(manufacturer);
        setPicture_url(picture_url);
      }
    } catch (error) {
      console.error("Error fetching model data:", error);
    }
  };

  useEffect(() => {
    fetchManufacturers();
    fetchModelData();
  }, [id]);

  const handleModelEditSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.manufacturer_id = currentManufacturer.id ? currentManufacturer.id : currentManufacturer;
    data.picture_url = picture_url;

    const modelUrl = `http://localhost:8100/api/models/${id}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(modelUrl, fetchConfig);

    if (response.ok) {
      setName('');
      setCurrentManufacturer(null);
      setPicture_url('');
      onClose();
      onModelEdited();
    }
  };

  const handleNameChange = (event) => {
    const { value } = event.target;
    setName(value);
  };

  const handleManufacturerChange = (event) => {
    const { value } = event.target;
    setCurrentManufacturer(value);
  };

  const handlePicture_urlChange = (event) => {
    const { value } = event.target;
    setPicture_url(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <form onSubmit={handleModelEditSubmit} id="create-vehicle-form">
        <h1>Edit Model</h1>
        <div className="mb-3">
          <input
            value={name}
            onChange={handleNameChange}
            placeholder="Model name"
            required
            type="text"
            name="name"
            id="name"
            className="form-control"
          />
        </div>
        <div className="mb-3">
          <input
            value={picture_url}
            onChange={handlePicture_urlChange}
            placeholder="Picture URL"
            type="text"
            name="picture_url"
            id="picture_url"
            className="form-control"
          />
        </div>
        <div className="mb-3">
          <select
            onChange={handleManufacturerChange}
            name="manufacturer"
            className="form-select"
            value={currentManufacturer ? currentManufacturer.id : ''}
          >
            {manufacturers.map((manufacturer) => {
              return (
                <option key={manufacturer.id} value={manufacturer.id}>
                  {manufacturer.name}
                </option>
              );
            })}
          </select>
        </div>
        <button className="btn btn-primary m-1">Submit</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default ModelEdit;
