import React, { useState, useEffect } from "react";

function ManufacturerEdit({ id, onClose, onManufacturerEdited }) {
  const [name, setManufacturer] = useState("");

  const fetchManufacturerData = async () => {
    try {
      const response = await fetch(`http://localhost:8100/api/manufacturers/${id}`);
      if (response.ok) {
        const data = await response.json();
        const { name } = data;
        setManufacturer(name);
      }
    } catch (error) {
      console.error("Error fetching manufacturer data:", error);
    }
  };

  useEffect(() => {
    fetchManufacturerData();
  }, [id]);

  const handleManufacturerEditSubmit = async (event) => {
    event.preventDefault();

    const manufacturerUrl = `http://localhost:8100/api/manufacturers/${id}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ name }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setManufacturer("");
      onClose();
      onManufacturerEdited();
    }
  };

  const handleManufacturerChange = (event) => {
    const { value } = event.target;
    setManufacturer(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Edit Manufacturer</h1>
      <form
        onSubmit={handleManufacturerEditSubmit}
        id="create-manufacturer"
        className="create-manufacturer"
      >
        <div className="mb-3">
          <input
            onChange={handleManufacturerChange}
            value={name}
            placeholder="Manufacturer name"
            required
            type="text"
            name="manufacturer"
            id="manufacturer"
            className="form-control"
          />
        </div>
        <button className="btn btn-primary m-1">Submit</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default ManufacturerEdit;
