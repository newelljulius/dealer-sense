import React, { useEffect, useState } from "react";

function AutomobileForm({ onClose, onAutomobileAdded }) {
  const [automobiles, setAutomobiles] = useState([]);
  const [models, setModels] = useState([]);
  const [model, setModel] = useState("");
  const [color, setColor] = useState("");
  const [vin, setVin] = useState("");
  const [year, setYear] = useState("");

  const fetchModels = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const fetchAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles");
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    fetchAutomobiles();
    fetchModels();
  }, []);

  const handleAutomobileFormSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.model_id = model;
    data.vin = vin;

    if (automobiles.some(auto => auto.vin === vin)) {
      alert("Error: This VIN already exists.");
      return;
    }

    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      setModel("");
      setYear("");
      setColor("");
      setVin("");
      onClose();
      onAutomobileAdded();
    }
  };

  const handleModelChange = (event) => {
    const { value } = event.target;
    setModel(value);
  };

  const handleColorChange = (event) => {
    const { value } = event.target;
    setColor(value);
  };

  const handleVinChange = (event) => {
    const { value } = event.target;
    setVin(value);
  };

  const handleYearChange = (event) => {
    const { value } = event.target;
    setYear(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <form onSubmit={handleAutomobileFormSubmit} id="create-model-form">
        <h1 className="card-title">Add Automobile</h1>
        <div className="col">
          <div className="mb-3">
            <input
              onChange={handleColorChange}
              required
              placeholder="Color"
              type="text"
              name="color"
              className="form-control"
              value={color}
            />
          </div>
        </div>
        <div className="col">
          <div className="mb-3">
            <input
              onChange={handleYearChange}
              required
              placeholder="Year"
              type="text"
              name="year"
              className="form-control"
              value={year}
            />
          </div>
        </div>
        <div className="col">
          <div className="mb-3">
            <input
              onChange={handleVinChange}
              required
              placeholder="VIN"
              type="text"
              name="vin"
              className="form-control"
              value={vin}
            />
          </div>
        </div>
        <div className="mb-3">
          <select
            onChange={handleModelChange}
            name="model"
            className="form-select"
            required
            value={model}
          >
            <option value="">Choose a model</option>
            {models.map((model) => {
              return (
                <option key={model.id} value={model.id}>
                  {model.name}
                </option>
              );
            })}
          </select>
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default AutomobileForm;
