import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from "react";
import AutomobileForm from "./AutomobileForm";
import AutomobileEdit from "./AutomobileEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);
  const [vinsSold, setVinsSold] = useState([]);
  const [isAutomobileFormOpen, setIsAutomobileFormOpen] = useState(false);
  const [isAutomobileEditOpen, setIsAutomobileEditOpen] = useState(false);
  const [selectedVin, setSelectedVin] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles");
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  const fetchSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setVinsSold(data.sales.map((sale) => sale.automobile.vin));
    }
  };

  useEffect(() => {
    fetchAutomobiles();
    fetchSales();
  }, []);

  const checkVinSold = (automobileVin) => {
    return vinsSold.includes(automobileVin);
  };

  const toggleAutomobileForm = () => {
    setIsAutomobileFormOpen(!isAutomobileFormOpen);
  };

  const toggleAutomobileEdit = () => {
    setIsAutomobileEditOpen(!isAutomobileEditOpen);
  };

  const handleAutomobileDelete = async (vin) => {
    try {
      const response = await fetch(`http://localhost:8100/api/automobiles/${vin}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchAutomobiles();
      } else {
        console.error(
          "Failed to delete automobile:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting automobile:", error);
    }
  };

  const onAutomobileAdded = () => {
    fetchAutomobiles();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  return (
    <div className="container custom-margin">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Automobile added successfully!
            </div>
          )}
            {isAutomobileFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <AutomobileForm onClose={toggleAutomobileForm}
                  onAutomobileAdded={onAutomobileAdded}/>
                </div>
              </div>
            )}
            {isAutomobileEditOpen && selectedVin && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <AutomobileEdit vin={selectedVin} onClose={toggleAutomobileEdit}
                  onAutomobileEdited={fetchAutomobiles}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
              <h1>Automobiles</h1>
              <button
                className="btn btn-success m-0"
                onClick={toggleAutomobileForm}
              >
                Add Automobile
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th className="col-2">VIN</th>
                    <th className="col-2">Color</th>
                    <th className="col-2">Year</th>
                    <th className="col-2">Model</th>
                    <th className="col-2">Manufacturer</th>
                    <th className="col-1">Sold</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {automobiles.map((automobile) => {
                    return (
                      <tr key={automobile.id}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td>{checkVinSold(automobile.vin) ? "Yes" : "No"}</td>
                        <td>
                          <button className="action-button" onClick={() => handleAutomobileDelete(automobile.vin)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                            className="action-button"
                            onClick={() => {
                              setSelectedVin(automobile.vin);
                              toggleAutomobileEdit();
                            }}
                          >
                            <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AutomobilesList;
