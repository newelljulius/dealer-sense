import React, { useEffect, useState } from "react";

function AutomobileEdit({ vin, onClose, onAutomobileEdited }) {
  const [models, setModels] = useState([]);
  const [currentModel, setCurrentModel] = useState(null);
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");

  const fetchModels = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const fetchAutomobileData = async () => {
    try {
      const response = await fetch(`http://localhost:8100/api/automobiles/${vin}`);
      if (response.ok) {
        const data = await response.json();
        const { color, year, model } = data;
        setColor(color);
        setYear(year);
        setCurrentModel(model);
      }
    } catch (error) {
      console.error("Error fetching automobile data:", error);
    }
  };

  useEffect(() => {
    fetchAutomobileData();
    fetchModels();
  }, [vin]);

  const handleAutomobileEditSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.model_id = currentModel.id ? currentModel.id : currentModel;
    data.vin = vin;

    const automobileUrl = `http://localhost:8100/api/automobiles/${vin}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      setCurrentModel(null);
      setYear("");
      setColor("");
      onClose();
      onAutomobileEdited();
    }
  };

  const handleModelChange = (event) => {
    const { value } = event.target;
    setCurrentModel(value);
  };

  const handleColorChange = (event) => {
    const { value } = event.target;
    setColor(value);
  };

  const handleYearChange = (event) => {
    const { value } = event.target;
    setYear(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <form onSubmit={handleAutomobileEditSubmit} id="create-model-form">
        <h1 className="card-title">Edit Automobile</h1>
        <div className="col">
          <div className="mb-3">
            <input
              onChange={handleColorChange}
              required
              placeholder="Color"
              type="text"
              name="color"
              className="form-control"
              value={color}
            />
          </div>
        </div>
        <div className="col">
          <div className="mb-3">
            <input
              onChange={handleYearChange}
              required
              placeholder="Year"
              type="text"
              name="year"
              className="form-control"
              value={year}
            />
          </div>
        </div>
        <div className="col">
          <div className="mb-3">
            <input
                type="text"
                className="form-control"
                value={vin}
                disabled
              />
          </div>
        </div>
        <div className="mb-3">
          <select
            onChange={handleModelChange}
            name="model"
            className="form-select"
            value={currentModel ? currentModel.id : ''}
          >
            {models.map((model) => {
              return (
                <option key={model.id} value={model.id}>
                  {model.name}
                </option>
              );
            })}
          </select>
        </div>
        <button className="btn btn-primary m-1">Submit</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default AutomobileEdit;
