import ManufacturersList from "./ManufacturersList";
import ModelsList from "./ModelsList";
import AutomobilesList from "./AutomobilesList";

function InventoryDashboard() {
  return (
    <div className="container mb-5">
      <h1 className="text-center mt-3 mb-3">Inventory Dashboard</h1>
      <div className="mt-3 mb-3">
        <ManufacturersList />
      </div>
      <div className="mt-3 mb-3">
        <ModelsList />
      </div>
      <div className="mt-3 mb-3">
        <AutomobilesList />
      </div>
    </div>
  );
}

export default InventoryDashboard;
