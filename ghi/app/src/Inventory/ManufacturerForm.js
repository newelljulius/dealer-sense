import React, { useState } from "react";

function ManufacturerForm({ onClose, onManufacturerAdded }) {
  const [name, setManufacturer] = useState("");

  const handleManufacturerFormSubmit = async (event) => {
    event.preventDefault();

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify({ name }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setManufacturer("");
      onClose();
      onManufacturerAdded();
    }
  };

  const handleManufacturerChange = (event) => {
    const { value } = event.target;
    setManufacturer(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Add Manufacturer</h1>
      <form
        onSubmit={handleManufacturerFormSubmit}
        id="create-manufacturer"
        className="create-manufacturer"
      >
        <div className="mb-3">
          <input
            onChange={handleManufacturerChange}
            value={name}
            placeholder="Manufacturer name"
            required
            type="text"
            name="manufacturer"
            id="manufacturer"
            className="form-control"
          />
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default ManufacturerForm;
