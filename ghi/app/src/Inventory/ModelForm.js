import React, { useEffect, useState } from "react";

function ModelForm({ onClose, onModelAdded }) {
  const [manufacturers, setManufacturers] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });

  const fetchManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const handleModelFormSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/models/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: "",
        picture_url: "",
        manufacturer_id: "",
      });
      onClose();
      onModelAdded();
    }
  };

  const handleFormChange = (event) => {
    const { value } = event.target;
    const inputName = event.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Add Model</h1>
      <form onSubmit={handleModelFormSubmit} id="create-vehicle-form">
        <div className="mb-3">
          <input
            value={formData.name}
            onChange={handleFormChange}
            placeholder="Model name"
            required
            type="text"
            name="name"
            id="name"
            className="form-control"
          />
        </div>
        <div className="mb-3">
          <input
            value={formData.picture_url}
            onChange={handleFormChange}
            placeholder="Picture URL"
            type="text"
            name="picture_url"
            id="picture_url"
            className="form-control"
          />
        </div>
        <div className="mb-3">
          <select
            onChange={handleFormChange}
            required
            name="manufacturer_id"
            id="manufacturer_id"
            className="form-select"
          >
            <option value="">Choose a manufacturer </option>
            {manufacturers.map((manufacturer) => {
              return (
                <option key={manufacturer.id} value={manufacturer.id}>
                  {" "}
                  {manufacturer.name}{" "}
                </option>
              );
            })}
          </select>
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default ModelForm;
