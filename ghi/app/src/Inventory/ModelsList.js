import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from "react";
import ModelForm from "./ModelForm";
import ModelEdit from "./ModelEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function ModelsList() {
  const [models, setModels] = useState([]);
  const [isModelFormOpen, setIsModelFormOpen] = useState(false);
  const [isModelEditOpen, setIsModelEditOpen] = useState(false);
  const [selectedModel, setSelectedModel] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchModels = async () => {
    const response = await fetch("http://localhost:8100/api/models");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  const toggleModelForm = () => {
    setIsModelFormOpen(!isModelFormOpen);
  };

  const toggleModelEdit = () => {
    setIsModelEditOpen(!isModelEditOpen);
  };

  const handleModelDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8100/api/models/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchModels();
      } else {
        console.error(
          "Failed to delete model:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting model:", error);
    }
  };

  const onModelAdded = () => {
    fetchModels();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Model added successfully!
            </div>
          )}
            {isModelFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ModelForm onClose={toggleModelForm}
                  onModelAdded={onModelAdded}/>
                </div>
              </div>
            )}
            {isModelEditOpen && selectedModel && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ModelEdit id={selectedModel} onClose={toggleModelEdit}
                  onModelEdited={fetchModels}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Models</h1>
            <button
                className="btn btn-success m-0"
                onClick={toggleModelForm}
              >
                Add Model
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th className="col-3">Name</th>
                    <th className="col-3">Manufacturer</th>
                    <th className="col-5">Picture</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {models.map((model) => {
                    return (
                      <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td>
                          <img
                            src={model.picture_url}
                            width="250px"
                            height="125px"
                            alt="Automobile"
                          />
                        </td>
                        <td>
                          <button className="action-button" onClick={() => handleModelDelete(model.id)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                            className="action-button"
                            onClick={() => {
                              setSelectedModel(model.id);
                              toggleModelEdit();
                            }}
                          >
                            <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModelsList;
