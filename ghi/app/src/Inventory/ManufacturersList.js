import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from "react";
import ManufacturerForm from "./ManufacturerForm";
import ManufacturerEdit from "./ManufacturerEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);
  const [isManufacturerFormOpen, setIsManufacturerFormOpen] = useState(false);
  const [isManufacturerEditOpen, setIsManufacturerEditOpen] = useState(false);
  const [selectedManufacturer, setSelectedManufacturer] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchManufacturers = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const toggleManufacturerForm = () => {
    setIsManufacturerFormOpen(!isManufacturerFormOpen);
  };

  const toggleManufacturerEdit = () => {
    setIsManufacturerEditOpen(!isManufacturerEditOpen);
  };

  const handleManufacturerDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8100/api/manufacturers/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchManufacturers();
      } else {
        console.error(
          "Failed to delete automobile:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting manufacturer:", error);
    }
  };

  const onManufacturerAdded = () => {
    fetchManufacturers();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Manufacturer added successfully!
            </div>
          )}
            {isManufacturerFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ManufacturerForm onClose={toggleManufacturerForm}
                  onManufacturerAdded={onManufacturerAdded}/>
                </div>
              </div>
            )}
            {isManufacturerEditOpen && selectedManufacturer && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ManufacturerEdit id={selectedManufacturer} onClose={toggleManufacturerEdit}
                  onManufacturerEdited={fetchManufacturers}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Manufacturers</h1>
            <button
              className="btn btn-success m-0"
              onClick={toggleManufacturerForm}
            >
              Add Manufacturer
            </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th className="col-11">Name</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <tr key={manufacturer.id}>
                        <td>{manufacturer.name}</td>
                        <td>
                          <button className="action-button" onClick={() => handleManufacturerDelete(manufacturer.id)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                            className="action-button"
                            onClick={() => {
                              setSelectedManufacturer(manufacturer.id);
                              toggleManufacturerEdit();
                            }}
                          >
                            <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturersList;
