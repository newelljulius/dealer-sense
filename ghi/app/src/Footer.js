import React from "react";

export default function Footer() {
  return (
    <footer className="footer py-3 bg-dark text-white text-center mt-auto">
      <div className="container">
        <span className="">
          &copy; {new Date().getFullYear()} DealerSense
        </span>
      </div>
    </footer>
  );
}
