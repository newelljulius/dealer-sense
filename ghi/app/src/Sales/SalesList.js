import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from "react";
import SalesForm from "./SalesForm";
import SalesEdit from "./SalesEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function SalesList() {
  const [sales, setSales] = useState([]);
  const [isSalesFormOpen, setIsSalesFormOpen] = useState(false);
  const [isSalesEditOpen, setIsSalesEditOpen] = useState(false);
  const [selectedSale, setSelectedSale] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchSales();
  }, []);

  const toggleSalesForm = () => {
    setIsSalesFormOpen(!isSalesFormOpen);
  };

  const toggleSalesEdit = () => {
    setIsSalesEditOpen(!isSalesEditOpen);
  };

  const handleSaleDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/sales/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchSales();
      } else {
        console.error(
          "Failed to delete sale:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting sale:", error);
    }
  };

  const onSaleAdded = () => {
    fetchSales();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };
  
  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Sale added successfully!
            </div>
          )}
          {isSalesFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <SalesForm onClose={toggleSalesForm}
                  onSaleAdded={onSaleAdded}/>
                </div>
              </div>
            )}
            {isSalesEditOpen && selectedSale && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <SalesEdit id={selectedSale} onClose={toggleSalesEdit}
                  onSaleEdited={fetchSales}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Sales</h1>
            <button
                className="btn btn-success m-0"
                onClick={toggleSalesForm}
              >
                Add Sale
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th className="col-3">Salesperson Employee ID</th>
                    <th className="col-2">Salesperson</th>
                    <th className="col-2">Customer</th>
                    <th className="col-2">VIN</th>
                    <th className="col-2">Price</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {sales.map((sale) => {
                    return (
                      <tr key={sale.id}>
                        <td>{sale.salesperson.id}</td>
                        <td>
                          {sale.salesperson.firstName}{" "}
                          {sale.salesperson.lastName}
                        </td>
                        <td>
                          {sale.customer.firstName} {sale.customer.lastName}
                        </td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.price}.00</td>
                        <td>
                          <button className="action-button" onClick={() => handleSaleDelete(sale.id)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                              className="action-button"
                              onClick={() => {
                                setSelectedSale(sale.id);
                                toggleSalesEdit();
                              }}
                            >
                              <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                            </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesList;
