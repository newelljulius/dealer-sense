import { useState, useEffect } from "react";

function CustomersEdit({ id, onClose, onCustomerEdited }) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const fetchCustomerData = async () => {
    try {
      const response = await fetch(`http://localhost:8090/api/customers/${id}`);
      if (response.ok) {
        const data = await response.json();
        const { firstName, lastName, address, phone_number } = data;
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setPhoneNumber(phone_number);
      }
    } catch (error) {
      console.error("Error fetching customer data:", error);
    }
  };

  useEffect(() => {
    fetchCustomerData();
  }, [id]);

  const handleCustomersEditSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.firstName = firstName;
    data.lastName = lastName;
    data.address = address;
    data.phone_number = phoneNumber;

    const url = `http://localhost:8090/api/customers/${id}`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setAddress("");
      setPhoneNumber("");
      onClose();
      onCustomerEdited ();
    }
  };

  const handleFirstNameChange = (event) => {
    const { value } = event.target;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const { value } = event.target;
    setLastName(value);
  };

  const handleAddressChange = (event) => {
    const { value } = event.target;
    setAddress(value);
  };

  const handlePhoneNumberChange = (event) => {
    const { value } = event.target;
    setPhoneNumber(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Edit Customer</h1>
      <form onSubmit={handleCustomersEditSubmit} id="create-salesperson-form">
        <div className="form-floating mb-3">
          <input
            value={firstName}
            onChange={handleFirstNameChange}
            placeholder="First name"
            required
            type="text"
            name="firstName"
            id="firstName"
            className="form-control"
          />
          <label htmlFor="firstName">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={lastName}
            onChange={handleLastNameChange}
            placeholder="Last Name"
            required
            type="text"
            name="lastName"
            id="lastName"
            className="form-control"
          />
          <label htmlFor="lastName">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={address}
            onChange={handleAddressChange}
            placeholder="Address"
            required
            type="text"
            name="address"
            id="address"
            className="form-control"
          />
          <label htmlFor="address">Address</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={phoneNumber}
            onChange={handlePhoneNumberChange}
            placeholder="Phone Number"
            required
            type="text"
            name="phone_number"
            id="phone_number"
            className="form-control"
          />
          <label htmlFor="phone_number">Phone Number</label>
        </div>
        <button className="btn btn-primary m-1">Submit</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default CustomersEdit;
