import React, { useEffect, useState } from "react";

function SalespersonHistory() {
  const [salesperson, setSalesperson] = useState("");

  const [salespeople, setSalespeople] = useState([]);
  const [sales, setSales] = useState([]);

  const handleSalespersonChange = (event) => {
    const { value } = event.target;
    setSalesperson(value);
  };

  const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  };

  const fetchSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchSalespeople();
    fetchSales();
  }, []);

  return (
    <div className="container custom-margin">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
            <div className=" m-3 p-3">
            <h1>Salesperson History</h1>
            <div className="mt-4">
              <select
                onChange={handleSalespersonChange}
                required
                id="salesperson"
                className="form-select"
                value={salesperson}
              >
                <option value="">Choose a salesperson</option>
                {salespeople.map((salesperson) => {
                  return (
                    <option
                      key={salesperson.id}
                      value={salesperson.id}
                    >
                      {salesperson.firstName} {salesperson.lastName}
                    </option>
                  );
                })}
              </select>
            </div>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  {sales
                    .filter((sale) => {
                      return sale.salesperson.id === salesperson;
                    })
                    .map((sale) => {
                      return (
                        <tr key={sale.id}>
                          <td>
                            {sale.salesperson.firstName}{" "}
                            {sale.salesperson.lastName}
                          </td>
                          <td>
                            {sale.customer.firstName} {sale.customer.lastName}
                          </td>
                          <td>{sale.automobile.vin}</td>
                          <td>${sale.price}.00</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalespersonHistory;
