import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from "react";
import CustomersForm from "./CustomersForm";
import CustomersEdit from "./CustomersEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function CustomersList() {
  const [customers, setCustomers] = useState([]);
  const [isCustomersFormOpen, setIsCustomersFormOpen] = useState(false);
  const [isCustomersEditOpen, setIsCustomersEditOpen] = useState(false);
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchCustomers();
  }, []);

  const toggleCustomersForm = () => {
    setIsCustomersFormOpen(!isCustomersFormOpen);
  };

  const toggleCustomersEdit = () => {
    setIsCustomersEditOpen(!isCustomersEditOpen);
  };

  const handleCustomerDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/customers/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchCustomers();
      } else {
        console.error(
          "Failed to delete customer:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting customer:", error);
    }
  };

  const onCustomerAdded = () => {
    fetchCustomers();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Customer added successfully!
            </div>
          )}
          {isCustomersFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <CustomersForm onClose={toggleCustomersForm}
                  onCustomerAdded={onCustomerAdded}/>
                </div>
              </div>
            )}
            {isCustomersEditOpen && selectedCustomer && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <CustomersEdit id={selectedCustomer} onClose={toggleCustomersEdit}
                  onCustomerEdited={fetchCustomers}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Customers</h1>
            <button
                className="btn btn-success m-0"
                onClick={toggleCustomersForm}
              >
                Add Customer
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th className="col-2">First Name</th>
                    <th className="col-2">Last Name</th>
                    <th className="col-2">Phone Number</th>
                    <th className="col-5">Address</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {customers.map((customer) => {
                    return (
                      <tr key={customer.id}>
                        <td>{customer.firstName}</td>
                        <td>{customer.lastName}</td>
                        <td>{customer.phone_number}</td>
                        <td>{customer.address}</td>
                        <td>
                          <button className="action-button" onClick={() => handleCustomerDelete(customer.id)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                              className="action-button"
                              onClick={() => {
                                setSelectedCustomer(customer.id);
                                toggleCustomersEdit();
                              }}
                            >
                              <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                            </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomersList;
