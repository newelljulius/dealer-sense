import { useState } from "react";

function SalespeopleForm({ onClose, onSalespersonAdded }) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleSalespeopleFormSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.firstName = firstName;
    data.lastName = lastName;

    const url = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      onClose();
      onSalespersonAdded();
    }
  };

  const handleFirstNameChange = (event) => {
    const { value } = event.target;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const { value } = event.target;
    setLastName(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Add Salesperson</h1>
      <form onSubmit={handleSalespeopleFormSubmit} id="create-salesperson-form">
        <div className="form-floating mb-3">
          <input
            value={firstName}
            onChange={handleFirstNameChange}
            placeholder="First name"
            required
            type="text"
            name="firstName"
            id="firstName"
            className="form-control"
          />
          <label htmlFor="firstName">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={lastName}
            onChange={handleLastNameChange}
            placeholder="Last Name"
            required
            type="text"
            name="lastName"
            id="lastName"
            className="form-control"
          />
          <label htmlFor="lastName">Last Name</label>
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default SalespeopleForm;