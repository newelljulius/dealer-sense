import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from "react";
import SalespeopleForm from "./SalespeopleForm";
import SalespeopleEdit from "./SalespeopleEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);
  const [isSalespeopleFormOpen, setIsSalespeopleFormOpen] = useState(false);
  const [isSalespeopleEditOpen, setIsSalespeopleEditOpen] = useState(false);
  const [selectedSalesperson, setSelectedSalesperson] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  const toggleSalespeopleForm = () => {
    setIsSalespeopleFormOpen(!isSalespeopleFormOpen);
  };

  const toggleSalespeopleEdit = () => {
    setIsSalespeopleEditOpen(!isSalespeopleEditOpen);
  };

  const handleSalespersonDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/salespeople/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchSalespeople();
      } else {
        console.error(
          "Failed to delete salesperson:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting salesperson:", error);
    }
  };

  const onSalespersonAdded = () => {
    fetchSalespeople();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };
  
  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Salesperson added successfully!
            </div>
          )}
          {isSalespeopleFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <SalespeopleForm onClose={toggleSalespeopleForm} 
                  onSalespersonAdded={onSalespersonAdded}/>
                </div>
              </div>
            )}
            {isSalespeopleEditOpen && selectedSalesperson && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <SalespeopleEdit id={selectedSalesperson} onClose={toggleSalespeopleEdit}
                  onSalespersonEdited={fetchSalespeople}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Salespeople</h1>
            <button
                className="btn btn-success m-0"
                onClick={toggleSalespeopleForm}
              >
                Add Salesperson
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th className="col-3">Employee ID</th>
                    <th className="col-3">First Name</th>
                    <th className="col-5">Last Name</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {salespeople.map((salesperson) => {
                    return (
                      <tr key={salesperson.id}>
                        <td>{salesperson.id}</td>
                        <td>{salesperson.firstName}</td>
                        <td>{salesperson.lastName}</td>
                        <td>
                          <button className="action-button" onClick={() => handleSalespersonDelete(salesperson.id)}>
                            <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                          </button>
                          <button
                              className="action-button"
                              onClick={() => {
                                setSelectedSalesperson(salesperson.id);
                                toggleSalespeopleEdit();
                              }}
                            >
                              <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                            </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalespeopleList;
