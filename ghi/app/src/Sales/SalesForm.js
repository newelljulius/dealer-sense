import { useState, useEffect } from "react";

function SalesForm({ onClose, onSaleAdded }) {
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [vin, setVin]= useState('');
  const [price, setPrice] = useState("");

  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [automobileVO, setAutomobileVO] = useState([]);

  const handleSalesFormSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.automobile = vin;
    data.customer = customer;
    data.salesperson = salesperson;
    data.price = price;

    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      setSalesperson("");
      setCustomer("");
      setVin("");
      setPrice("");
      onClose();
      onSaleAdded();
    } else {
      console.error("Error:", response.status, response.statusText);
      const responseData = await response.json();
      console.error("Response Data:", responseData);
    }
  };

  const handleSalespersonChange = (event) => {
    const { value } = event.target;
    setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
    const { value } = event.target;
    setCustomer(value);
  };

  const handleVinChange = (event) => {
    const { value } = event.target;
    setVin(value);
  };

  const handlePriceChange = (event) => {
    const { value } = event.target;
    setPrice(value);
  };

  const fetchAutomobileVO = async() => {
    const response = await fetch('http://localhost:8100/api/automobiles/');

    if (response.ok) {
        const data = await response.json();
        setAutomobileVO(data.autos.filter(auto => !auto.sold))
    } else {
        console.error(response);
    }
}


  const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  };

  const fetchCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchAutomobileVO();
    fetchSalespeople();
    fetchCustomers();
  }, []);

  return (
    <div className="shadow p-4 mt-4">
      <h1 className="text-left">Add Sale</h1>
      <form id="create-appointment-form" onSubmit={handleSalesFormSubmit}>
        <label htmlFor="vin">Automobile VIN</label>
        <div className="mb-3">
          <select
            onChange={handleVinChange}
            required
            id="vin"
            className="form-select"
            value={vin}
          >
            <option value="">Choose an automobile VIN</option>
            {automobileVO.map((automobileVO) => {
              return (
                <option key={automobileVO.id} value={automobileVO.id}>
                  {automobileVO.vin}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="salesperson">Salesperson</label>
        <div className="mb-3">
          <select
            onChange={handleSalespersonChange}
            required
            id="salesperson"
            className="form-select"
            value={salesperson}
          >
            <option value="">Choose a salesperson</option>
            {salespeople.map((salesperson) => {
              return (
                <option key={salesperson.id} value={salesperson.id}>
                  {salesperson.firstName} {salesperson.lastName}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="customer">Customer</label>
        <div className="mb-3">
          <select
            onChange={handleCustomerChange}
            required
            id="customer"
            className="form-select"
            value={customer}
          >
            <option value="">Choose a customer</option>
            {customers.map((customer) => {
              return (
                <option key={customer.id} value={customer.id}>
                  {customer.firstName} {customer.lastName}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="price">Price</label>
        <div className="mb-3">
          <input
            onChange={handlePriceChange}
            required
            type="number"
            name="price"
            id="price"
            className="form-control"
            value={price}
          />
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default SalesForm;
