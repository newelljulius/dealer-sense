import SalespeopleList from "./SalespeopleList";
import CustomersList from "./CustomersList";
import SalesList from "./SalesList";
import SalespersonHistory from "./SalespersonHistory";

function SalesDashboard() {
  return (
    <div className="container mb-5">
      <h1 className="text-center mt-3 mb-3">Sales Dashboard</h1>
      <div className="mt-3 mb-3">
        <SalespeopleList />
      </div>
      <div className="mt-3 mb-3">
        <CustomersList />
      </div>
      <div className="mt-3 mb-3">
        <SalesList />
      </div>
      <div className="mt-3 mb-3">
        <SalespersonHistory />
      </div>
    </div>
  );
}

export default SalesDashboard;
