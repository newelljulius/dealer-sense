import { useState, useEffect } from "react";

function SalesEdit({ id, onClose, onSaleEdited }) {
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [automobile, setAutomobile] = useState("");
  const [price, setPrice] = useState("");
  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [automobileVO, setAutomobileVO] = useState([]);

  const [currentSalesperson, setCurrentSalesperson] = useState(null);
  const [currentCustomer, setCurrentCustomer] = useState(null);
  const [currentAutomobile, setCurrentAutomobile] = useState(null);

  const fetchSaleData = async () => {
    try {
      const response = await fetch(`http://localhost:8090/api/sales/${id}`);
      if (response.ok) {
        const data = await response.json();
        const { salesperson, customer, automobile, price } = data;
        setCurrentSalesperson(salesperson);
        setCurrentCustomer(customer);
        setCurrentAutomobile(automobile);
        setPrice(price);
        setAutomobile(automobile.vin);
      }
    } catch (error) {
      console.error("Error fetching automobile data:", error);
    }
  };

  const handleSalesEditSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.automobile = automobile.id ? automobile.id : automobile;
    data.customer = customer;
    data.salesperson = salesperson;
    data.price = price;

    const url = `http://localhost:8090/api/sales/${id}`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      setSalesperson("");
      setCustomer("");
      setAutomobile("");
      setPrice("");
      onClose();
      onSaleEdited();
    } else {
      console.error("Error:", response.status, response.statusText);
      const responseData = await response.json();
      console.error("Response Data:", responseData);
    }
  };

  const handleSalespersonChange = (event) => {
    const { value } = event.target;
    setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
    const { value } = event.target;
    setCustomer(value);
  };

  const handleAutomobileChange = (event) => {
    const { value } = event.target;
    console.log("Selected Automobile VIN:", value);
    setAutomobile(value);
  };

  const handlePriceChange = (event) => {
    const { value } = event.target;
    setPrice(value);
  };

  const fetchAutomobileVO = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutomobileVO(data.autos);
    } else {
      console.error(response);
    }
  };

  const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    } else {
      console.error(response);
    }
  };

  const fetchCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchSaleData();
    fetchAutomobileVO();
    fetchSalespeople();
    fetchCustomers();
  }, [id]);

  return (
    <div className="shadow p-4 mt-4">
      <h1 className="text-left">Edit Sale</h1>
      <form id="create-appointment-form" onSubmit={handleSalesEditSubmit}>
        <label htmlFor="automobile">Automobile VIN</label>
        <div className="mb-3">
          <select
            onChange={handleAutomobileChange}
            required
            id="automobile"
            className="form-select"
            value={currentAutomobile ? currentAutomobile.id : ''}
          >
            <option value="">Choose an automobile VIN</option>
            {automobileVO.map((auto) => {
              return (
                <option key={auto.id} value={auto.id}>
                  {auto.vin}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="salesperson">Salesperson</label>
        <div className="mb-3">
          <select
            onChange={handleSalespersonChange}
            required
            type="text"
            id="salesperson"
            className="form-select"
            value={currentSalesperson ? currentSalesperson.id : ''}
          >
            <option value="">Choose a salesperson</option>
            {salespeople.map((salesperson) => {
              return (
                <option key={salesperson.id} value={salesperson.id}>
                  {salesperson.firstName} {salesperson.lastName}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="customer">Customer</label>
        <div className="mb-3">
          <select
            onChange={handleCustomerChange}
            required
            type="text"
            id="customer"
            className="form-select"
            value={currentCustomer ? currentCustomer.id : ''}
          >
            <option value="">Choose a customer</option>
            {customers.map((customer) => {
              return (
                <option key={customer.id} value={customer.id}>
                  {customer.firstName} {customer.lastName}
                </option>
              );
            })}
          </select>
        </div>
        <label htmlFor="price">Price</label>
        <div className="mb-3">
          <input
            onChange={handlePriceChange}
            required
            type="number"
            name="price"
            id="price"
            className="form-control"
            value={price}
          />
        </div>
        <button className="btn btn-primary m-1">Submit</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default SalesEdit;
