import React, { useEffect, useState } from "react";

function ServiceAppointmentForm({ onClose, onServiceAppointmentAdded }) {
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState("");
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [reason, setReason] = useState("");

  const handleTechnicianChange = (event) => {
    const { value } = event.target;
    setTechnician(value);
  };

  const handleVinChange = (event) => {
    const { value } = event.target;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const { value } = event.target;
    setCustomer(value);
  };

  const handleDateChange = (event) => {
    const { value } = event.target;
    setDate(value);
  };

  const handleTimeChange = (event) => {
    const { value } = event.target;
    setTime(value);
  };

  const handleReasonChange = (event) => {
    const { value } = event.target;
    setReason(value);
  };

  const handleServiceAppointmentFormSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.status = "created";
    data.customer = customer;
    data.reason = reason;
    data.date_time = `${date} ${time}`;
    data.technician = technician;

    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setReason("");
      setTechnician("");
      onClose();
      onServiceAppointmentAdded();
    }
  };

  const fetchTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchTechnicians();
  }, []);

  return (
    <div className="shadow p-4 mt-4">
      <h1 className="text-left">Create a service appointment</h1>
      <form id="create-appointment-form" onSubmit={handleServiceAppointmentFormSubmit}>
        <label htmlFor="name">Automobile VIN</label>
        <div className="mb-3">
          <input
            onChange={handleVinChange}
            required
            type="number"
            name="vin"
            id="vin"
            className="form-control"
            value={vin}
          />
        </div>

        <label htmlFor="customer">Customer</label>
        <div className="mb-3">
          <input
            onChange={handleCustomerChange}
            required
            type="text"
            name="customer"
            id="customer"
            className="form-control"
            value={customer}
          />
        </div>

        <label htmlFor="date">Date</label>
        <div className="mb-3">
          <input
            onChange={handleDateChange}
            required
            type="date"
            name="date"
            id="date"
            className="form-control"
            value={date}
          />
        </div>

        <label htmlFor="time">Time</label>
        <div className="mb-3">
          <input
            onChange={handleTimeChange}
            required
            type="time"
            name="time"
            id="time"
            className="form-control"
            value={time}
          />
        </div>

        <label htmlFor="technician">Technician</label>
        <div className="mb-3">
          <select
            onChange={handleTechnicianChange}
            required
            id="technician"
            className="form-select"
            value={technician}
          >
            <option value="">Technician</option>
            {technicians.map((technician) => {
              return (
                <option
                  key={technician.id}
                  value={technician.id}
                >
                  {technician.firstName + " " + technician.lastName}
                </option>
              );
            })}
          </select>
        </div>

        <label htmlFor="reason">Reason</label>
        <div className="mb-3">
          <input
            onChange={handleReasonChange}
            required
            type="text"
            name="reason"
            id="reason"
            className="form-control"
            value={reason}
          />
        </div>

        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default ServiceAppointmentForm;
