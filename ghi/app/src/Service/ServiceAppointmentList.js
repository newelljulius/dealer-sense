import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from "react";
import ServiceAppointmentForm from "./ServiceAppointmentForm";
import ServiceAppointmentEdit from "./ServiceAppointmentEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";
import finishIcon from "../icons/finish.png";

function ServiceAppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [inventoryVin, setInventoryVin] = useState([]);
  const [vinSearch, setVinSearch] = useState("");
  const [vinResult, setVinResult] = useState([]);
  const [isServiceFormOpen, setIsServiceFormOpen] = useState(false);
  const [isServiceEditOpen, setIsServiceEditOpen] = useState(false);
  const [selectedAppointment, setSelectedAppointment] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchAppointments = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  const fetchTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const fetchInventoryVin = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setInventoryVin(data.autos);
    }
  };

  useEffect(() => {
    fetchAppointments();
    fetchTechnicians();
    fetchInventoryVin();
  }, []);

  const handleVinSearch = (event) => {
    setVinSearch(event.target.value);
  };

  const handleSearchButton = () => {
    const search = appointments.filter(
      (appointment) => appointment.vin === vinSearch
    );
    setVinResult(search);
  };

  const onServiceAppointmentAdded = () => {
    fetchAppointments();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  const handleFinishAppointment = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/finish`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ status: "finished" }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setAppointments((prevAppointments) =>
        prevAppointments.map((appt) =>
          appt.id === appointment.id ? { ...appt, status: "finished" } : appt
        )
      );
    }
  };

  const technicianName = (technicianId) => {
    const technician = technicians.find(
      (tech) => tech.id === technicianId
    );
    return technician ? `${technician.firstName} ${technician.lastName}` : "";
  };

  const checkVinInInventory = (appointmentVin) => {
    return inventoryVin.some((auto) => auto.vin === appointmentVin);
  };

  const toggleServiceForm = () => {
    setIsServiceFormOpen(!isServiceFormOpen);
  };

  const toggleServiceEdit = () => {
    setIsServiceEditOpen(!isServiceEditOpen);
  };

  const handleServiceAppointmentDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchAppointments();
      } else {
        console.error(
          "Failed to delete appointment:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting appointment:", error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Appointment added successfully!
            </div>
          )}
            {isServiceFormOpen && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ServiceAppointmentForm onClose={toggleServiceForm}
                  onServiceAppointmentAdded={onServiceAppointmentAdded}/>
                </div>
              </div>
            )}
            {isServiceEditOpen && selectedAppointment && (
              <div className="form-overlay">
                <div className="m-3 form-popup">
                  <ServiceAppointmentEdit id={selectedAppointment} onClose={toggleServiceEdit}
                  onServiceAppointmentEdited={fetchAppointments}/>
                </div>
              </div>
            )}
            <div className="d-flex m-3 p-3 justify-content-between align-items-center">
              <h1>Service Appointments</h1>
              <button
                className="btn btn-success m-0"
                onClick={toggleServiceForm}
              >
                Add Appointment
              </button>
            </div>
            <div className="d-flex flex-column">
              <div className="row mb-3">
                <div className="col">
                  <div className="input-group">
                    <input
                      type="text"
                      id="upcomingAppointmentSearch"
                      placeholder="Search by VIN"
                      value={vinSearch}
                      onChange={handleVinSearch}
                      className="form-control"
                    />
                    <div className="input-group-append">
                      <button
                        onClick={handleSearchButton}
                        type="button"
                        className="btn btn-outline-secondary"
                      >
                        Search
                      </button>
                    </div>
                  </div>
                </div>
                <div className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th className="col-3">VIN</th>
                        <th className="col-1">Is VIP?</th>
                        <th className="col-1">Customer</th>
                        <th className="col-1">Date</th>
                        <th className="col-1">Time</th>
                        <th className="col-1">Technician</th>
                        <th className="col-1">Reason</th>
                        <th className="col-1">Status</th>
                        <th className="col-2">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {vinResult.length > 0
                        ? vinResult
                          .filter(
                            (appointment) =>
                              appointment.status !== 'finished' && appointment.status !== 'cancelled'
                          )
                          .map((appointment) => (
                            <tr key={appointment.id}>
                              <td>{appointment.vin}</td>
                              <td>
                                {checkVinInInventory(appointment.vin)
                                  ? "Yes"
                                  : "No"}
                              </td>
                              <td>{appointment.customer}</td>
                              <td>
                                {new Date(
                                  appointment.date_time
                                ).toLocaleDateString()}
                              </td>
                              <td>
                                {new Date(
                                  appointment.date_time
                                ).toLocaleTimeString()}
                              </td>
                              <td>{technicianName(appointment.technician)}</td>
                              <td>{appointment.reason}</td>
                              <td>{appointment.status}</td>
                              <td>
                                <button className="action-button" onClick={() => handleServiceAppointmentDelete(appointment.id)}>
                                  <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                                </button>
                                <button className="action-button" onClick={() => handleFinishAppointment(appointment)}>
                                  <img src={finishIcon} alt="Finish" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                                </button>
                                <button
                                  className="action-button"
                                  onClick={() => {
                                    setSelectedAppointment(appointment.id);
                                    toggleServiceEdit();
                                  }}
                                >
                                  <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                                </button>
                              </td>
                            </tr>
                          ))
                        : appointments
                          .filter(
                            (appointment) =>
                              appointment.status !== 'finished' && appointment.status !== 'cancelled'
                          )
                          .map((appointment) => (
                            <tr key={appointment.id}>
                              <td>{appointment.vin}</td>
                              <td>
                                {checkVinInInventory(appointment.vin)
                                  ? "Yes"
                                  : "No"}
                              </td>
                              <td>{appointment.customer}</td>
                              <td>
                                {new Date(
                                  appointment.date_time
                                ).toLocaleDateString()}
                              </td>
                              <td>
                                {new Date(
                                  appointment.date_time
                                ).toLocaleTimeString()}
                              </td>
                              <td>{technicianName(appointment.technician)}</td>
                              <td>{appointment.reason}</td>
                              <td>{appointment.status}</td>
                              <td>
                                <button className="action-button" onClick={() => handleServiceAppointmentDelete(appointment.id)}>
                                  <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                                </button>
                                <button className="action-button" onClick={() => handleFinishAppointment(appointment)}>
                                  <img src={finishIcon} alt="Finish" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                                </button>
                                <button
                                  className="action-button"
                                  onClick={() => {
                                    setSelectedAppointment(appointment.id);
                                    toggleServiceEdit();
                                  }}
                                >
                                  <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                                </button>
                              </td>
                            </tr>
                          ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ServiceAppointmentList;
