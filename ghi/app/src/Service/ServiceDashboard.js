import ServiceAppointmentList from "./ServiceAppointmentList";
import TechnicianList from "./TechnicianList";
import ServiceHistoryList from "./ServiceHistoryList"

function ServiceDashboard() {
  return (
    <div className="container mb-5">
      <h1 className="text-center mt-3 mb-3">Service Dashboard</h1>
      <div className="mt-3 mb-3">
        <ServiceAppointmentList />
      </div>
      <div className="mt-3 mb-3">
        <ServiceHistoryList />
      </div>
      <div className="mt-3 mb-3">
        <TechnicianList />
      </div>
    </div>
  );
}

export default ServiceDashboard;
