import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from "react";
import TechnicianForm from "./TechnicianForm"
import TechnicianEdit from "./TechnicianEdit";
import deleteIcon from "../icons/delete.png";
import editIcon from "../icons/edit.png";

function TechnicianList() {
  const [technicians, setTechnician] = useState([]);
  const [isTechnicianFormOpen, setIsTechnicianFormOpen] = useState(false);
  const [isTechnicianEditOpen, setIsTechnicianEditOpen] = useState(false);
  const [selectedTechnician, setSelectedTechnician] = useState(null);
  const [createSuccessAlert, setCreateSuccessAlert] = useState(false);

  const fetchTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnician(data.technicians);
    }
  };

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const toggleTechnicianForm = () => {
    setIsTechnicianFormOpen(!isTechnicianFormOpen);
  };

  const toggleTechnicianEdit = () => {
    setIsTechnicianEditOpen(!isTechnicianEditOpen);
  };

  const handleTechnicianDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/technicians/${id}`, {
        method: "DELETE",
        credentials: "include",
      });

      if (response.ok) {
        fetchTechnicians();
      } else {
        console.error(
          "Failed to delete technician:",
          response.status,
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error while deleting technician:", error);
    }
  };

  const onTechnicianAdded = () => {
    fetchTechnicians();
    setCreateSuccessAlert(true);
    setTimeout(() => {
      setCreateSuccessAlert(false);
    }, 3000);
  };

  return (
    <div className="container custom-margin">
      <div className="row">
        <div className="justify-content-center col-12">
          <div className="shadow p-4 mt-4">
          {createSuccessAlert && (
            <div className="alert alert-success fade show" role="alert">
              Technician added successfully!
            </div>
          )}
          {isTechnicianFormOpen && (
            <div className="form-overlay">
              <div className="m-3 form-popup">
                <TechnicianForm onClose={toggleTechnicianForm}
                onTechnicianAdded={onTechnicianAdded}/>
              </div>
            </div>
          )}
          {isTechnicianEditOpen && selectedTechnician && (
            <div className="form-overlay">
              <div className="m-3 form-popup">
                <TechnicianEdit id={selectedTechnician} onClose={toggleTechnicianEdit}
                onTechnicianEdited={fetchTechnicians}/>
              </div>
            </div>
          )}
          <div className="d-flex m-3 p-3 justify-content-between align-items-center">
            <h1>Technicians</h1>
            <button
                className="btn btn-success m-0"
                onClick={toggleTechnicianForm}
              >
                Add Technician
              </button>
            </div>
            <div className="table-responsive">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th className="col-3">Employee ID</th>
                    <th className="col-3">First Name</th>
                    <th className="col-4">Last Name</th>
                    <th className="col-1">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {technicians.map((technician) => (
                    <tr key={technician.id}>
                      <td>{technician.id}</td>
                      <td>{technician.firstName}</td>
                      <td>{technician.lastName}</td>
                      <td>
                        <button className="action-button" onClick={() => handleTechnicianDelete(technician.id)}>
                          <img src={deleteIcon} alt="Delete" style={{ maxWidth: '24px', maxHeight: '24px' }}/>
                        </button>
                        <button
                            className="action-button"
                            onClick={() => {
                              setSelectedTechnician(technician.id);
                              toggleTechnicianEdit();
                            }}
                          >
                            <img src={editIcon} alt="Edit" style={{ maxWidth: '24px', maxHeight: '24px' }} />
                          </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TechnicianList;
