import React, { useState } from "react";

function TechnicianForm({ onClose, onTechnicianAdded }) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleTechnicianFormSubmit = async (event) => {
    event.preventDefault();

    const data = {
      firstName: firstName,
      lastName: lastName,
    };

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      onClose();
      onTechnicianAdded();
    }
  };

  const handleFirstNameChange = (event) => {
    const { value } = event.target;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const { value } = event.target;
    setLastName(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Add a Technician</h1>
      <form onSubmit={handleTechnicianFormSubmit} id="create-location-form">
        <div className="form-floating mb-3">
          <input
            value={firstName}
            onChange={handleFirstNameChange}
            placeholder="First Name"
            required
            type="text"
            name="firstName"
            id="firstName"
            className="form-control"
          />
          <label htmlFor="firstName">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={lastName}
            onChange={handleLastNameChange}
            placeholder="Last Name"
            required
            type="text"
            name="lastName"
            id="lastName"
            className="form-control"
          />
          <label htmlFor="lastName">Last Name</label>
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default TechnicianForm;
