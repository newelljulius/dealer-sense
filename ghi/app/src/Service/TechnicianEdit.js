import React, { useState, useEffect } from "react";

function TechnicianEdit({ id, onClose, onTechnicianEdited }) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const fetchTechnicianData = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/technicians/${id}`);
      if (response.ok) {
        const data = await response.json();
        const { firstName, lastName } = data;
        setFirstName(firstName);
        setLastName(lastName);
      }
    } catch (error) {
      console.error("Error fetching technician data:", error);
    }
  };

  const handleTechnicianEditSubmit = async (event) => {
    event.preventDefault();

    const data = {
      firstName: firstName,
      lastName: lastName,
    };

    const technicianUrl = `http://localhost:8080/api/technicians/${id}`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      onClose();
      onTechnicianEdited();
    }
  };

  useEffect(() => {
    fetchTechnicianData();
  }, [id]);

  const handleFirstNameChange = (event) => {
    const { value } = event.target;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const { value } = event.target;
    setLastName(value);
  };

  return (
    <div className="shadow p-4 mt-4">
      <h1>Edit Technician</h1>
      <form onSubmit={handleTechnicianEditSubmit} id="create-location-form">
        <div className="form-floating mb-3">
          <input
            value={firstName}
            onChange={handleFirstNameChange}
            placeholder="First Name"
            required
            type="text"
            name="firstName"
            id="firstName"
            className="form-control"
          />
          <label htmlFor="firstName">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={lastName}
            onChange={handleLastNameChange}
            placeholder="Last Name"
            required
            type="text"
            name="lastName"
            id="lastName"
            className="form-control"
          />
          <label htmlFor="lastName">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            value={id}
            type="number"
            name="number"
            id="id"
            className="form-control"
            disabled
          />
          <label htmlFor="id">ID</label>
        </div>
        <button className="btn btn-primary m-1">Create</button>
        <button className="btn btn-secondary m-1" onClick={onClose}>
          Cancel
        </button>
      </form>
    </div>
  );
}

export default TechnicianEdit;
