import './index.css';

function MainPage() {
  return (
    <div>
      <div className="hero-image">
        <div className="hero-text">
          <h1 className="display-5 fw-bold">DealerSense</h1>
            <p className="lead mb-4">The premiere solution for automobile dealership management.</p>
        </div>
      </div>
      <div className='content-container'>
        <div className='album py-5 bg-light'>
        <div className="container custom-margin">
            <div className='row'>
              <div className="col-md-4">
                <div className="card mb-4 box-shadow">
                  <div className='ratio ratio-3x2'>
                    <img className="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                    alt="Thumbnail [100%x250]"
                    src="https://images.pexels.com/photos/7144176/pexels-photo-7144176.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" data-holder-rendered="true" />
                  </div>
                  <div className="card-body">
                    <p className="card-text text-justify">Manage your inventory of automobiles from leading manufacturers. Showcase your vehicles to attract potential buyers.</p>
                    <div className="d-flex justify-content-center">
                      <a href="/inventory-dashboard">
                        <button type="button" className="btn btn-primary">Inventory Dashboard</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="card mb-4 box-shadow">
                  <div className='ratio ratio-3x2'>
                    <img className="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                    alt="Thumbnail [100%x250]"
                    src="https://images.pexels.com/photos/4173189/pexels-photo-4173189.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" data-holder-rendered="true" />
                  </div>
                  <div className="card-body">
                    <p className="card-text text-justify">Stay up-to-date with sales and customer information. Analyze your sales to enhance your dealership's success.</p>
                    <div className="d-flex justify-content-center">
                      <a href="/sales-dashboard">
                        <button type="button" className="btn btn-primary">Sales Dashboard</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="card mb-4 box-shadow">
                  <div className='ratio ratio-3x2'>
                    <img className="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                    alt="Thumbnail [100%x250]"
                    src="https://images.pexels.com/photos/3807386/pexels-photo-3807386.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2" data-holder-rendered="true" />
                  </div>
                  <div className="card-body">
                    <p className="card-text text-justify">Efficiently schedule and track service appointments, guaranteeing exceptional customer care and vehicle maintenance.</p>
                    <div className="d-flex justify-content-center">
                      <a href="/service-dashboard">
                        <button type="button" className="btn btn-primary">Service Dashboard</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
